<?php
/**
 * Backend filter form
 *
 * @author    Szymon Bluma
 * @copyright Szymon [BlueMan] Bluma
 * @link      http://www.blueman.pl
 */
class MrBlue_Form_Filter extends Twitter_Bootstrap_Form_Inline
{
    /**
     * Auth
     *
     * @var Zend_Auth
     * @access protected
     */
    protected $_auth = null;

    /**
     * Action name
     *
     * @var string
     * @access protected
     */
    protected $_action;

    /**
     * Request params
     *
     * @var array
     * @access protected
     */
    protected $_params = array();

    /**
     * Construct
     *
     * @return \MrBlue_Form_Filter
     */
    public function __construct()
    {
        if (Zend_Registry::isRegistered('auth')) {
            $this->_auth = Zend_Registry::get('auth');
        }

        $this->_action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
        $this->_params = Zend_Controller_Front::getInstance()->getRequest()->getParams();

        $this->setName('form');
        $this->setMethod('post');
        $this->setAction($_SERVER['REQUEST_URI']);
        $this->_addClassNames('well');

        parent::__construct();
    }

    /**
     * Get identity
     *
     * @return Zend_Auth User identity
     */
    protected function getIdentity()
    {
        return $this->_auth->getIdentity();
    }

    /**
     * Get Zend_Acl
     *
     * @return Zend_Acl
     */
    protected function getAcl()
    {
        return Zend_Registry::get('acl');
    }
}