<?php

class MrBlue_Helper_System
{

    /**
     * Recursive delete folder
     *
     * @param string $dir dir path
     */
    public static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        self::rrmdir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            unset($objects);
            rmdir($dir);
        }
    }

    /**
     * Get client IP address
     *
     * @return string ip address
     */
    public static function getIp()
    {
        if (getenv('HTTP_CLIENT_IP')) {
            return getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            return getenv('HTTP_X_FORWARDED_FOR');
        }

        return getenv('REMOTE_ADDR');
    }

    /**
     * Convert array to obcject
     *
     * @param array $array Array
     *
     * @return object Object from passed array
     */
    public static function array2object($array)
    {
        if (is_array($array)) {
            $obj = new StdClass();

            foreach ($array as $key => $val) {
                $obj->$key = $val;
            }
        } else {
            $obj = $array;
        }

        return $obj;
    }
}