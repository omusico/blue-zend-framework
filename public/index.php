<?php

/* Application Time Start */
define('ATS', array_sum(explode(' ',microtime())));

iconv_set_encoding("internal_encoding", "UTF-8");
iconv_set_encoding("output_encoding", "UTF-8");
iconv_set_encoding("input_encoding", "UTF-8");

// Define application environment
define('APPLICATION_ENV', 'development');

// Short directory separator
define('DS', DIRECTORY_SEPARATOR);

// Define website address
define('LOCATION', 'http://' . $_SERVER['HTTP_HOST'] . '/');

// Define path to main directory
defined('SOURCE_PATH')
    || define('SOURCE_PATH', realpath(dirname(dirname(__FILE__))));

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', SOURCE_PATH . '/application');

// Define path to public directory
defined('PUBLIC_PATH')
    || define('PUBLIC_PATH', SOURCE_PATH . '/public');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
	'/var/www/lib/zend/1.12.3/',
    realpath(SOURCE_PATH . '/library'),
    get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);
unset($autoloader);

require_once 'functions.php';
initDirStructure();

$config = new Zend_Config_Ini(
    APPLICATION_PATH . '/configs/application.ini',
    APPLICATION_ENV,
    array('allowModifications' => true)
);

$database = new Zend_Config_Ini(
    APPLICATION_PATH . '/configs/database.ini',
    APPLICATION_ENV
);

$config->merge($database);

// Create application, bootstrap, and run
$application = new Zend_Application(APPLICATION_ENV, $config);
$application->bootstrap()
    ->run();