<?php

class SuperAdmin_IndexController extends MrBlue_Controller_Action_SuperAdmin
{
    public function indexAction()
    {
        if ( !$this->_auth->hasIdentity() ) {
            // redirect
            return $this->_helper->redirector('login');
        }
    }

    public function loginAction()
    {
        $this->_helper->layout->setLayout('login');

        // user logged?
        if ($this->_auth->hasIdentity()) {
            // redirect
            return $this->_helper->redirector('index');
        }

        // login form
        $loginForm = new SuperAdmin_Form_SignIn();

        // is POST request?
        if ( !empty($this->aPost) ) {
            // form is valid?
            if ($loginForm->isValid($this->aPost)) {
                // data from form
                $email = $loginForm->getValue('email');
                $password = $loginForm->getValue('password');

                // auth adapter
                $authAdapter = $this->getAuthAdapter();

                // pass data to the adapter
                $authAdapter->setIdentity($email)
                    ->setCredential($password);

                $result = $this->_auth->authenticate($authAdapter);

                // auth data is valid?
                if ($result->isValid()) {
                    // get user data
                    $userInfo = $authAdapter->getResultRowObject(array('id', 'email', 'login_dt', 'login_ip'), 'pass');

                    // save user data in session
                    $authStorage = $this->_auth->getStorage();
                    $authStorage->write($userInfo);

                    // save user login date
                    //$oUserModel = new Application_Model_User();
                    //$oUserModel->fetchItem($userInfo->id);
                    //$oUserModel->setLoginDt(new Zend_Db_Expr('NOW()'));
                    //$oUserModel->saveItem();

                    // message
                    $this->_flashMessenger->addMessage(array('success', 'You have been signed in.'));

                    // redirect
                    return $this->_helper->redirector('index');
                }
                else
                {
                    // wrong credentials
                    if (!empty($password)) {
                        // message
                        $this->_flashMessenger->addMessage(array('error', 'Please enter correct data!'));
                    }

                    // redirect
                    return $this->_helper->redirector('login');
                }
            }
        }

        // pass data to the view
        $this->view->headTitle('Sign In');
        $this->view->form = $loginForm;
    }

    public function logoutAction()
    {
        // user logged?
        if ($this->_auth->hasIdentity()) {
            // clear user data
            $this->_auth->clearIdentity();

            // message
            $this->_flashMessenger->addMessage(array('success', 'You have been signed out.'));
        }

        // redirect
        return $this->_helper->redirector('login');
    }

    /**
     * Auth adapter
     *
     * @return \Zend_Auth_Adapter_DbTable
     */
    protected function getAuthAdapter()
    {
        // auth adapter using database adapter
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Registry::get('db'));

        // auth adapter settings
        $authAdapter->setTableName('user_admins')
            ->setIdentityColumn('email')
            ->setCredentialColumn('pass')
            ->setCredentialTreatment('SHA1(CONCAT("'.$this->config->salt.'",?,salt))');

        return $authAdapter;
    }

}
